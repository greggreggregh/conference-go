from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    query = {"query": f"{city}, {state}"}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers, params=query)
    content = json.loads(response.content)
    try:
        picture_url = content["photos"][0]["src"]["original"]
        return {"picture_url": picture_url}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_coordinates(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    query = {
        "q": f"{city}, {state}",
        "limit": 3,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=query)
    content = json.loads(response.content)
    coordinates = {
        "lat": content[0]["lat"],
        "lon": content[0]["lon"],
    }
    return coordinates


def get_weather(city, state):
    coordinates = get_coordinates(city, state)
    url = "https://api.openweathermap.org/data/2.5/weather"
    query = {
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
        "lat": coordinates.get("lat"),
        "lon": coordinates.get("lon"),
    }
    response = requests.get(url, params=query)
    content = json.loads(response.content)
    weather = {
        "temp": content["main"]["temp"],
        "description": content["weather"][0]["description"],
    }
    return weather
